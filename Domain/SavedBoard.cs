﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GameEngine;

namespace Domain
{
    public class SavedBoard
    {
        public int SavedBoardId { get; set; }
        
        public int BoardHeight { get; set; }

        public int BoardWidth { get; set; }

        [Required] public string Board { get; set; } = default!;

        [Required] public int ElementsInBoard { get; set; } = default!;

        [Required] public bool GameDone { get; set; } = default!;

        [Required] public bool FirstPlayerMove { get; set; } = default!;
        
        [Required] public bool AgainstComputer { get; set; } = default!;


        public override string ToString()
        {
            return $"{Board}";
        }
    }
}