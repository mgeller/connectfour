﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class GameSetting
    {

        public int GameSettingId { get; set; }
        
        public int BoardHeight { get; set; } = 6;

        public int BoardWidth { get; set; } = 7;

        [Required] public bool AgainstComputer { get; set; } = default!;


    }
}