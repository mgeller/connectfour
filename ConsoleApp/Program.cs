﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleUI;
using DAL;
using Domain;
using GameEngine;
using MenuSystem;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    public class Program
    {
        private static AppDbContext _ctx;

        private static SavedBoardJson _saved;
        
        public static void Main(string[] args)
        {
            
            Console.Clear();

            Console.WriteLine(5/2);
            var dbOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(@"Data Source=C:\Users\mgell\csharp2019fall\Connect 4\WebApp\Saveddb.db").Options;
            
            _ctx = new AppDbContext(dbOptions);
            
            const string defaultFile = "default.json";
            
            _saved = GameConfigHandler.LoadGameState(defaultFile);


            Console.WriteLine();
            Console.WriteLine("Welcome to Connect4 Game!");

            var gameMenu = new Menu(1)
            {
                Title = "Start a new game of Connect 4",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "1", new MenuItem()
                        {
                            Title = "Start a new game",
                            CommandToExecute = NewGame
                        }
                    },
                    {
                        "2", new MenuItem()
                        {
                            Title = "Load game",
                            CommandToExecute = LoadGame
                        }
                    }
                }
            };
            

            var menu0 = new Menu(0)
            {
                Title = "Connect 4 Main Menu",
                MenuItemsDictionary = new Dictionary<string, MenuItem>()
                {
                    {
                        "S", new MenuItem()
                        {
                            Title = "Start game",
                            CommandToExecute = gameMenu.Run
                        }
                    },
                    {
                        "J", new MenuItem()
                        {
                            Title = "Set Game Settings",
                            CommandToExecute = SaveBoard
                        }
                    }
                }
            };
            
            menu0.Run();
        }

        static string SaveBoard()
        {
            Console.Clear();

            var boardHeight = 0;
            var boardWidth = 0;
            var cancel = false;
            var prevGameSetting = _ctx.GameSettings.FirstOrDefault();
            if (_ctx.GameSettings.Count() != 0 && prevGameSetting != null)
            {
                _ctx.GameSettings.Remove(prevGameSetting);
            }

            var againstPc = AgainstComputer();
            Console.WriteLine("Would you like default board size? (Y/N)");
            Console.Write("> ");
            var answer = Console.ReadLine()?.Trim().ToUpper();
            if (answer.Equals("Y"))
            {
                _ctx.GameSettings.Add(new GameSetting()
                {
                    BoardHeight = 6,
                    BoardWidth = 7,
                    AgainstComputer = againstPc
                });
                _ctx.SaveChanges();
                return "";
            }

            (boardHeight, cancel) = GetUserInput("Enter board height", 4, 20);
            if (cancel)
            {
                return "";
            }

            (boardWidth, cancel) = GetUserInput("Enter board width", 4, 20);
            if (cancel)
            {
                return "";
            }

            _ctx.GameSettings.Add(new GameSetting()
            {
                BoardHeight = boardHeight,
                BoardWidth = boardWidth,
                AgainstComputer = againstPc
            });
            _ctx.SaveChanges();
            Console.WriteLine("Your board settings are saved");
            return "";
            
        }

        static (int result, bool wasCanceled) GetUserInput(string prompt, int minInt, int maxInt,
            string cancelValue = "X")
        {
            do
            {
                Console.WriteLine(prompt);
                Console.WriteLine($"To cancel input enter: {cancelValue}");
                Console.Write("> ");

                var userLine = Console.ReadLine();

                if (userLine != null && userLine.ToUpper() == cancelValue)
                {
                    return (-1, true);
                }

                if (int.TryParse(userLine, out var userInt))
                {
                    if (userInt > maxInt || userInt < minInt)
                    {
                        Console.WriteLine($"Input must be in range of {minInt} - {maxInt}");
                    }
                    else
                    {
                        return (userInt, false);
                    }
                    
                }
            } while (true);
        }

        static string NewGame()
        {
            var currentSetting = _ctx.GameSettings.FirstOrDefault();
            if (currentSetting == null)
            {
                return "";
            }

            var game = new Game(currentSetting.BoardHeight, currentSetting.BoardWidth, currentSetting.AgainstComputer);
            
            var gameDone = false;

            ExecuteGame(game, gameDone);

            return "X";
        }

        static string LoadGame()
        {
            Console.Clear();
            if (!_ctx.SavedBoards.Any())
            {
                Console.WriteLine("There are no saved games yet!");
                return "";
            }

            var validFileName = false;
            var fileName = "";
            do
            {
                Console.Clear();
                var allSavedGames = PrintOutAllSavedGames();

                Console.WriteLine("Please choose JSON file to load game from");
                Console.Write("> ");
                
                fileName = Console.ReadLine()?.Trim();
                
                if (fileName != null && ContainsJson(allSavedGames[fileName]))
                {
                    fileName = allSavedGames[fileName];
                    validFileName = true;
                }
                else
                {
                    Console.WriteLine("No matching games found. Please try Again!");
                }
            } while (!validFileName);
            
            _saved = GameConfigHandler.LoadGameState(fileName);

            var savedBoard = new SavedBoard();
            foreach (var board in _ctx.SavedBoards)
            {
                if (board.Board == fileName)
                {
                    savedBoard = board;
                }
            }
            var elementCount = savedBoard.ElementsInBoard;
            var game = new Game(savedBoard.BoardHeight, savedBoard.BoardWidth, _saved,
                savedBoard.AgainstComputer, elementCount);

            var gameDone = savedBoard.GameDone;
            game._firstPlayerMove = savedBoard.FirstPlayerMove;

            ExecuteGame(game, gameDone);

            return "X";
        }

        static string ExecuteGame(Game game, bool gameDone)
        {
            do
            {
                Console.Clear();
                Connect4UI.PrintGameBoard(game);
                var userColumn = 0;
                var canceled = false;

                if (game._againstComputer && game._firstPlayerMove)
                {
                    var opponent = new AIOpponent(game.BoardHeight, game.BoardWidth, game.GetBoard());
                    userColumn = opponent.ChooseColumn();
                    Console.WriteLine(userColumn);
                }
                else
                {
                    var prompt = "Please enter column number ";
                    if (game._firstPlayerMove)
                    {
                        prompt += "(Red player)";
                    }
                    else
                    {
                        prompt += "(Yellow player)";
                    }
                    (userColumn, canceled) = GetUserInput(prompt, 0,
                        game.BoardWidth - 1);
                    if (canceled)
                    {
                        Console.Clear();
                        Console.WriteLine("Would you like to save current game? (Y/N)");
                        Console.Write("> ");
                        var userInput = Console.ReadLine();
                        if (userInput != null && userInput.ToUpper() == "Y")
                        {
                            SaveCurrentGame(game, gameDone);
                        }
                        break;
                    }
                }
                game.Move(userColumn);
                
                if (game.WinningState())
                {
                    Console.Clear();
                    Connect4UI.PrintGameBoard(game);
                    if (game._firstPlayerMove)
                    {
                        Console.WriteLine("Yellow player won!");
                    }
                    else
                    {
                        Console.WriteLine("Red player won!");
                    }
                    gameDone = true; 
                }
                else if (game.BoardFull())
                {
                    Console.Clear();
                    Connect4UI.PrintGameBoard(game);
                    Console.WriteLine("Game Over");
                    gameDone = true;
                }

            } while (!gameDone);
            return "Test";
        }

        static void SaveCurrentGame(Game game, bool gameDone)
        {
            var jsonBoard = game.ConvertBoardToJagged();
            _saved.Board = jsonBoard;

            Console.WriteLine("Please specify your JSON string where you want to save");
            Console.Write("> ");
            var fileName = Console.ReadLine()?.Trim();
            if (ContainsJson(fileName))
            {
                Console.WriteLine("Would you like to overwrite given file? (Y/N)");
                Console.Write("> ");
                var answer = Console.ReadLine()?.Trim().ToUpper();
                if (answer == "Y")
                {
                    RemoveFile(fileName);
                }
            }
            GameConfigHandler.SaveGameState(_saved, fileName);
            Console.WriteLine("Saved!");

            _ctx.SavedBoards.Add(new SavedBoard()
            {
                BoardHeight = game.BoardHeight,
                BoardWidth = game.BoardWidth,
                ElementsInBoard = game.ElementCount,
                FirstPlayerMove = game._firstPlayerMove,
                GameDone = gameDone,
                Board = fileName,
                AgainstComputer = game._againstComputer
            });
            _ctx.SaveChanges();
        }

        static bool ContainsJson(string fileName)
        {
            foreach (var file in _ctx.SavedBoards)
            {
                if (file.ToString().Equals(fileName))
                {
                    return true;
                }
            }
            return false;
        }

        static void RemoveFile(string filename)
        {
            foreach (var savedBoard in _ctx.SavedBoards)
            {
                if (savedBoard.ToString().Equals(filename))
                {
                    _ctx.SavedBoards.Remove(savedBoard);
                    _ctx.SaveChanges();
                }
            }
        }

        static bool AgainstComputer()
        {
            var opponentChosen = false;
            var againstComp = false;
            do
            {
                Console.Clear();
                Console.WriteLine("Would you like to play against Computer (Press 0) or another player (Press 1)?");
                Console.Write("> ");
                var answer = Console.ReadLine()?.Trim();

                if (answer == "0")
                {
                    againstComp = true;
                    opponentChosen = true;
                }
                else if (answer == "1")
                {
                    opponentChosen = true;
                }
                else
                {
                   Console.Clear();
                   Console.WriteLine("Input can't be other than 0 or 1! Please try again!");
                }
            } while (!opponentChosen);

            return againstComp;
        }

        static Dictionary<string, string> PrintOutAllSavedGames()
        {
            var allSavedBoardsDict = new Dictionary<string, string>();
            var number = 1;
            foreach (var savedBoard in _ctx.SavedBoards)
            {
                if (number > _ctx.SavedBoards.Count())
                {
                    break;
                }
                Console.WriteLine($"{number} | {savedBoard}");
                allSavedBoardsDict.Add(number.ToString(), savedBoard.Board);
                number++;
            }

            return allSavedBoardsDict;
        }
    }
}