﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace GameEngine
{
    public class GameConfigHandler
    {
        [Required] private const string FileName = "gamesettings.json";
        [Required] private const string ListOfFiles = "listoffiles.json";

        public static void SaveConfig(GameSettingsJson settingsJson, string fileName = FileName)
        {
            var jsonOption = new JsonSerializerOptions()
            {
                AllowTrailingCommas = true,
                WriteIndented = true
            };
            using (var writer = System.IO.File.CreateText(fileName))
            {
                var jsonString = JsonSerializer.Serialize(settingsJson, jsonOption);
                writer.Write(jsonString);
            }
        }

        public static GameSettingsJson LoadConfig(string fileName = FileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                var jsonString = System.IO.File.ReadAllText(fileName);
                var res = JsonSerializer.Deserialize<GameSettingsJson>(jsonString);
                
                return res;
            }
            return new GameSettingsJson();
        }

        public static void SaveGameState(SavedBoardJson savedGame, [Required] string fileName)
        {
            var jsonOption = new JsonSerializerOptions()
            {
                AllowTrailingCommas = true,
                WriteIndented = true
            };
            using (var writer = System.IO.File.
                CreateText(@"C:\Users\mgell\csharp2019fall\Connect 4\WebApp\Jsons\" + fileName))
            {
                var jsonString = JsonSerializer.Serialize(savedGame, jsonOption);
                writer.Write(jsonString);
                Console.WriteLine(jsonString);
            }
        }

        public static SavedBoardJson LoadGameState([Required] string fileName)
        {
            if (System.IO.File.Exists(@"C:\Users\mgell\csharp2019fall\Connect 4\WebApp\Jsons\" + fileName))
            {
                var jsonString = System.IO.File
                    .ReadAllText(@"C:\Users\mgell\csharp2019fall\Connect 4\WebApp\Jsons\" + fileName);
                var result = JsonSerializer.Deserialize<SavedBoardJson>(jsonString);

                return result;
            }
            return new SavedBoardJson();
        }

        public static void SaveFile(FileStorage fileStorage, string fileName = ListOfFiles)
        {
            var jsonOption = new JsonSerializerOptions()
            {
                AllowTrailingCommas = true,
                WriteIndented = true
            };
            using (var writer = System.IO.File.CreateText(fileName))
            {
                var jsonString = JsonSerializer.Serialize(fileStorage, jsonOption);
                writer.Write(jsonString);
                Console.WriteLine(jsonString);
            }
        }

        public static FileStorage LoadFile(string fileName = ListOfFiles)
        {
            if (System.IO.File.Exists(fileName))
            {
                var jsonString = System.IO.File.ReadAllText(fileName);
                var result = JsonSerializer.Deserialize<FileStorage>(jsonString);

                return result;
            }
            return new FileStorage();
        }

    }
}