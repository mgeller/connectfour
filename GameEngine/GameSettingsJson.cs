﻿using System.Collections.Generic;

namespace GameEngine
{
    public class GameSettingsJson
    {

        public int BoardHeight { get; set; } = 6;

        public int BoardWidth { get; set; } = 7;
        

    }
}