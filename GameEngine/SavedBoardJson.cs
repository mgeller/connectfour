﻿using System.ComponentModel.DataAnnotations;

namespace GameEngine
{
    public class SavedBoardJson
    {

        [Required] public CellState[][] Board { get; set; } = default!;

    }
}