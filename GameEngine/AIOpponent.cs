﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GameEngine
{
    public class AIOpponent
    {
        
        [Required] private CellState[,] Board { get; set; } = default!;

        [Required] public int BoardWidth { get; } = default!;

        [Required] public int BoardHeight { get; } = default!;

        public AIOpponent(int boardHeight, int boardWidth, CellState[,] board)
        {
            Board = board;
            BoardHeight = boardHeight;
            BoardWidth = boardWidth;
        }
        
        public int ChooseColumn()
        {
            var random = new Random();
            int randomColumn;
            if (Block() != -1)
            {
                randomColumn = Block();
                return randomColumn;
            }
            do
            {
                randomColumn = random.Next(BoardWidth);
            } while (!CanUseColumn(randomColumn));
            
            return randomColumn;
        }

        private bool CanUseColumn(int column)
        {
            var canUseColumn = false;
            for (int row = BoardHeight - 1; row >= 0; row--)
            {
                if (Board[row, column] == CellState.Empty)
                {
                    canUseColumn = true;
                    break;
                }
            }

            return canUseColumn;
        }

        private int Block()
        {
            for (int row = BoardHeight - 1; row >= 0; row--)
            {
                for (int col = 0; col < BoardWidth; col++)
                {
                    var currentElement = Board[row, col];
                                        
                    if (currentElement != CellState.Empty) continue;
                    if (NeedToBlockRow(currentElement, row, col))
                    {
                        return col;
                    }

                    if (NeedToBlockCol(currentElement, row, col))
                    {
                        return col;
                    }
                }
            }

            return -1;
        }

        private bool NeedToBlockRow(CellState currentElement, int row, int col)
        {
            if (col + 3 < BoardWidth)
            {
                if (currentElement != Board[row, col + 1] && 
                    Board[row, col + 1] == Board[row, col + 2] && 
                    Board[row, col + 2] == Board[row, col + 3])
                {
                    return true;
                }
            }
            if (col - 3 >= 0)
            {
                if (currentElement != Board[row, col - 1] && 
                    Board[row, col - 1] == Board[row, col - 2] && 
                    Board[row, col - 2] == Board[row, col - 3])
                {
                    return true;
                }
            }
            if (col + 1 < BoardWidth && col - 1 >= 0)
            {
                if (currentElement != Board[row, col + 1] && 
                    Board[row, col + 1] == Board[row, col - 1])
                {
                    return true;
                }
            }

            return false;
        }
        
        private bool NeedToBlockCol(CellState currentElement, int row, int col)
        {
            if (row - 3 >= 0)
            {
                if (currentElement != Board[row - 1, col] && 
                    Board[row - 1, col] == Board[row - 2, col] && 
                    Board[row - 2, col] == Board[row - 3, col])
                {
                    return true;
                }
            }

            return false;
        }
    }
}