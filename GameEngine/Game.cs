﻿using System;
using System.ComponentModel.DataAnnotations;


namespace GameEngine
{
    public class Game
    {
        [Required] private SavedBoardJson _savedGame = default!;
        [Required] private CellState[,] Board { get; set; } = default!;

        [Required] public int BoardWidth { get; } = default!;

        [Required] public int BoardHeight { get; } = default!;

        public int ElementCount { get; set; }

        public bool _againstComputer;

        public bool _firstPlayerMove;
        

        public Game(int boardHeight, int boardWidth, bool againstComputer)
        {
            if (boardHeight < 4 || boardWidth < 4)
            {
                throw new Exception("Board size needs to be at least 4x4");
            }

            _againstComputer = againstComputer;
            BoardHeight = boardHeight;
            BoardWidth = boardWidth;
            ElementCount = 0;
            Board = new CellState[BoardHeight,BoardWidth];
        }

        public Game(int boardHeight, int boardWidth, SavedBoardJson savedBoard, bool againstComputer, int elementCount)
        {
            _savedGame = savedBoard;
            _againstComputer = againstComputer;
            ElementCount = elementCount;

            BoardHeight = boardHeight;
            BoardWidth = boardWidth;
            Board = new CellState[BoardHeight, BoardWidth];
            for (int rowIn = 0; rowIn < BoardHeight; rowIn++)
            {
                for (int colIn = 0; colIn < BoardWidth; colIn++)
                {
                    Board[rowIn, colIn] = _savedGame.Board[rowIn][colIn];
                }
            }
        }

        public CellState[,] GetBoard()
        {
            var result = new CellState[BoardHeight, BoardWidth];
            Array.Copy(Board, result, Board.Length);
            return result;
        }

        public void Move(int column)
        {
            for (int row = BoardHeight - 1; row >= 0 ; row--)
            {
                if (Board[row, column] != CellState.Empty)
                {
                    continue;
                }

                Board[row, column] = _firstPlayerMove ? CellState.Red : CellState.Yellow;
                _firstPlayerMove = !_firstPlayerMove;
                ElementCount++;
                break;
            }
        }

        public CellState[][] ConvertBoardToJagged()
        {
            CellState[][] jsonBoard = new CellState[BoardHeight][];
            CellState[] row = new CellState[BoardWidth];
            var count = 0;
            for (int i = 0; i < BoardHeight; i++)
            { 
                foreach (var cellState in Board)
                {
                    row[count] = cellState;
                    count += 1;
                    if (count == BoardWidth)
                    {
                        jsonBoard[i] = row;
                                    
                        row = new CellState[BoardWidth];
                        count = 0;
                        i++;
                    }
                }
            }

            return jsonBoard;
        }

        public bool WinningState()
        {
            for (int rowIndex = BoardHeight - 1; rowIndex >= 0; rowIndex--)
            {
                for (int colIndex = 0; colIndex < BoardWidth; colIndex++)
                {
                    var currentElement = Board[rowIndex, colIndex];
                    
                    if (currentElement == CellState.Empty) continue;
                    
                    if (colIndex + 3 < BoardWidth)
                    {
                        if (currentElement == Board[rowIndex, colIndex + 1] &&
                            currentElement == Board[rowIndex, colIndex + 2] &&
                            currentElement == Board[rowIndex, colIndex + 3])
                        {
                            return true;
                        }
                    }

                    if (rowIndex - 3 >= 0)
                    {
                        if (currentElement == Board[rowIndex - 1, colIndex] && 
                            currentElement == Board[rowIndex - 2, colIndex] && 
                            currentElement == Board[rowIndex - 3, colIndex])
                        {
                            return true;
                        }
                    }

                    if (rowIndex - 3 >= 0 && colIndex + 3 < BoardWidth)
                    {
                        if (currentElement == Board[rowIndex - 1, colIndex + 1] &&
                            currentElement == Board[rowIndex - 2, colIndex + 2] &&
                            currentElement == Board[rowIndex - 3, colIndex + 3])
                        {
                            return true;
                        }
                    }
                    
                    if (rowIndex - 3 >= 0 && colIndex - 3 >= 0)
                    {
                        if (currentElement == Board[rowIndex - 1, colIndex - 1] &&
                            currentElement == Board[rowIndex - 2, colIndex - 2] &&
                            currentElement == Board[rowIndex - 3, colIndex - 3])
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public bool BoardFull()
        {
            var boardFull = ElementCount == BoardHeight * BoardWidth;

            return boardFull;
        }
    }
}