﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using WebApp.Pages.Game;

namespace WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        
        [BindProperty] public MainMenuModel.StartingChoice Choice { get; set; } = new MainMenuModel.StartingChoice();

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
        
        public ActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                if (Choice.ButtonValue == "Connect4 Main Menu")
                {
                    return RedirectToPage("./Game/MainMenu");
                }

            }
            return Page();
        }
    }
}