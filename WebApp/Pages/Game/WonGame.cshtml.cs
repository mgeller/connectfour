﻿using System.Linq;
using System.Threading.Tasks;
using DAL;
using GameEngine;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Game
{
    public class WonGameModel : PageModel
    {
        private readonly AppDbContext _ctx;

        private SavedBoardJson _saved;

        public GameEngine.Game Game { get; set; }
        
        public int GameId { get; set; }

        public WonGameModel(AppDbContext ctx)
        {
            _ctx = ctx;
            
        }

        public async Task<ActionResult> OnGet(int? gameId)
        {
            if (gameId == null)
            {
                return RedirectToPage("./StartNewGame");
            }

            GameId = gameId.Value;

            var savedBoard = _ctx.SavedBoards.First(x => x.SavedBoardId == GameId);
            _saved = GameConfigHandler.LoadGameState(savedBoard.Board);

            Game = new GameEngine.Game(savedBoard.BoardHeight, savedBoard.BoardWidth, _saved,
                savedBoard.AgainstComputer, savedBoard.ElementsInBoard);
            Game._firstPlayerMove = savedBoard.FirstPlayerMove;

            _ctx.SavedBoards.Remove(savedBoard);
            await _ctx.SaveChangesAsync();

            return Page();
        }
    }
}