﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc.RazorPages;
using GameEngine;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Pages.Game
{
    public class StartNewGameModel : PageModel
    {
        private readonly AppDbContext _ctx;
        
        [BindProperty] public GameSetting GameSetting { get; set; } = new GameSetting();
        [BindProperty] public MainMenuModel.StartingChoice Choice { get; set; } = 
            new MainMenuModel.StartingChoice();

        public StartNewGameModel(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        
        public void OnGet()
        {
            
        }

        public async Task<ActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var prevGameSetting = _ctx.GameSettings.FirstOrDefault();
                if (_ctx.GameSettings.Count() != 0 && prevGameSetting != null)
                {
                    _ctx.GameSettings.Remove(prevGameSetting);
                }
                
                var againstComputer = Choice.ButtonValue == "VS PC";
                GameSetting.AgainstComputer = againstComputer;

                var game = new GameEngine.Game(GameSetting.BoardHeight, GameSetting.BoardWidth, GameSetting.AgainstComputer);
                var dateAndTime = DateTime.Now.ToString("yyyy/MM/dd HH.mm tt");
                var savedBoardJson = new SavedBoardJson();
                savedBoardJson.Board = game.ConvertBoardToJagged();
                GameConfigHandler.SaveGameState(savedBoardJson, dateAndTime);
                
                
                _ctx.GameSettings.Add(GameSetting);
                var savedBoard = new SavedBoard()
                {
                    BoardHeight = GameSetting.BoardHeight,
                    BoardWidth = GameSetting.BoardWidth,
                    Board = dateAndTime,
                    ElementsInBoard = 0,
                    GameDone = false,
                    FirstPlayerMove = false,
                    AgainstComputer = game._againstComputer
                };
                _ctx.SavedBoards.Add(savedBoard);
                await _ctx.SaveChangesAsync();

                return RedirectToPage("./PlayGame", new {gameId = savedBoard.SavedBoardId});
            }

            return Page();
        }
    }
}