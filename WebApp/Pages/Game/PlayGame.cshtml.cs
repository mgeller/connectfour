﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using GameEngine;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Game
{
    public class PlayGameModel : PageModel
    {
        private readonly AppDbContext _ctx;

        private SavedBoardJson _saved;

        public GameEngine.Game Game { get; set; }
        
        public int GameId { get; set; }

        public PlayGameModel(AppDbContext ctx)
        {
            _ctx = ctx;
            
        }
        
        public async Task<ActionResult> OnGet(int? gameId, int? row, int? col)
        {
            if (gameId == null)
            {
                return RedirectToPage("./StartNewGame");
            }

            GameId = gameId.Value;
            
            var savedBoard = _ctx.SavedBoards.First(x => x.SavedBoardId == GameId);
            _saved = GameConfigHandler.LoadGameState(savedBoard.Board);
            
            Game = new GameEngine.Game(savedBoard.BoardHeight, savedBoard.BoardWidth, _saved,
                savedBoard.AgainstComputer, savedBoard.ElementsInBoard);
            Game._firstPlayerMove = savedBoard.FirstPlayerMove;
            
            if (col != null && row != null)
            {
                
                Game.Move(col.Value);
                SaveCurrentGameState(savedBoard);
                await _ctx.SaveChangesAsync();
                if (Game.WinningState() || Game.BoardFull())
                {
                    return RedirectToPage("./WonGame", new {gameId = savedBoard.SavedBoardId});
                }

                if (Game._againstComputer)
                {
                    var opponent = new AIOpponent(Game.BoardHeight, Game.BoardWidth, Game.GetBoard());
                    var opponentChoice = opponent.ChooseColumn();
                    Game.Move(opponentChoice);
                    SaveCurrentGameState(savedBoard);
                    await _ctx.SaveChangesAsync();
                }
                
                if (Game.WinningState() || Game.BoardFull())
                {
                    return RedirectToPage("./WonGame", new {gameId = savedBoard.SavedBoardId});
                }
            }
            return Page();
        }
        
        public void SaveCurrentGameState(SavedBoard savedBoard)
        {
            var savedBoardJson = new SavedBoardJson();
            savedBoardJson.Board = Game.ConvertBoardToJagged();
            GameConfigHandler.SaveGameState(savedBoardJson, savedBoard.Board);
            savedBoard.FirstPlayerMove = Game._firstPlayerMove;
            savedBoard.ElementsInBoard = Game.ElementCount;
            _ctx.Update(savedBoard);
        }
    }
}