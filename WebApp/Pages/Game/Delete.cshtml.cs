﻿using System.Linq;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages.Game
{
    public class DeleteModel : PageModel
    {
        private readonly DAL.AppDbContext _ctx;
        
        [BindProperty] public SavedBoard SavedBoard { get; set; }

        public DeleteModel(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        
        public async Task<ActionResult> OnGet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SavedBoard = await _ctx.SavedBoards.FirstOrDefaultAsync(x => x.SavedBoardId == id);

            if (SavedBoard == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<ActionResult> OnPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            SavedBoard = await _ctx.SavedBoards.FindAsync(id);

            if (SavedBoard != null)
            {
                _ctx.SavedBoards.Remove(SavedBoard);
                await _ctx.SaveChangesAsync();
            }

            return RedirectToPage("./LoadGame");
        }
    }
}