﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DAL;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Game
{
    public class LoadGameModel : PageModel
    {
        private readonly AppDbContext _ctx;
        public int ElementCounter { get; set; } = 0;
            
        [BindProperty] public MainMenuModel.StartingChoice Choice { get; set; } = new MainMenuModel.StartingChoice();

        public Dictionary<int, string> GetAllBoards()
        {
            Dictionary<int, string> boardDict = new Dictionary<int, string>();
            foreach (var savedBoard in _ctx.SavedBoards)
            {
                boardDict.Add(savedBoard.SavedBoardId, savedBoard.Board);
            }

            return boardDict;
        }

        public int AddElementCounter()
        {
            ElementCounter++;
            return ElementCounter;
        }

        public LoadGameModel(AppDbContext ctx)
        {
            _ctx = ctx;
        }
        
        public void OnGet()
        {
            
        }

        public  ActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            return RedirectToPage("./PlayGame");
        }
    }
}