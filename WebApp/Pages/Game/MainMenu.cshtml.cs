﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages.Game
{
    public class MainMenuModel : PageModel
    {
    
        [BindProperty] public StartingChoice Choice { get; set; } = new StartingChoice();
        
        public void OnGet()
        {
            
        }

        public ActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                if (Choice.ButtonValue == "Start New Game")
                {
                    return RedirectToPage("./StartNewGame");
                }

                if (Choice.ButtonValue == "Load Game")
                {
                    return RedirectToPage("./LoadGame");
                }
                
            }
            return Page();
        }

        public class StartingChoice
        {
            public string ButtonValue { get; set; }
        }
    }
}