﻿using System;
using System.ComponentModel;
using GameEngine;

namespace ConsoleUI
{
    public static class Connect4UI
    {
        private static readonly string _verticalSeparator = "|";
        private static readonly string _topBottomSeparator = "_";

        public static void PrintGameBoard(Game game)
        {
            
            var gameBoard = game.GetBoard();
            var columnNumbers = " ";
            for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
            {
                columnNumbers += " " + xIndex + "  ";
            }
            Console.WriteLine(columnNumbers);
            for (int yIndex = 0; yIndex < game.BoardHeight; yIndex++)
            {
                var line = "";
                if (yIndex == 0)
                {
                    line = " ";
                    for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
                    {
                        line = line + _topBottomSeparator + _topBottomSeparator +  _topBottomSeparator +" ";
                    }
                    Console.WriteLine(line);
                    line = "";
                }
                for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
                {
                    line = line + _verticalSeparator + " " + GetSingleState(gameBoard[yIndex, xIndex]) + " ";
                    if (xIndex == game.BoardWidth - 1)
                    {
                        line += _verticalSeparator;
                    }

                }
                Console.WriteLine(line);
                if (yIndex <= game.BoardHeight - 1)
                {
                    line = "";
                    for (int xIndex = 0; xIndex < game.BoardWidth; xIndex++)
                    {
                        line = line + _topBottomSeparator + _topBottomSeparator + _topBottomSeparator;
                        if (xIndex == 0)
                        {
                            line = _verticalSeparator + line;
                        } else if (xIndex == game.BoardWidth - 1)
                        {
                            line += _verticalSeparator;
                        }
                        if (xIndex < game.BoardWidth - 1)
                        {
                            line += _verticalSeparator;
                        }
                    }
                    Console.WriteLine(line);
                }
            }

        }

        public static String GetSingleState(CellState state)
        {
            switch (state)
            {
                case CellState.Empty:
                    return " ";
                case CellState.Red:
                    return "R";
                case CellState.Yellow:
                    return "Y";
                default:
                    throw new InvalidEnumArgumentException("Unknown enum option!");
            }
        }
    }
}